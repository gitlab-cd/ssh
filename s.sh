test -n "$1" && SSH_PRIVATE_KEY=$1
test -n "$2" && SSH_HOST_NAME=$2
test -n "$3" && SSH_HOST_PORT=$3
test -z "$SSH_PRIVATE_KEY" && echo Fatal : missing variable SSH_PRIVATE_KEY && exit 1
test -z "$SSH_HOST_NAME" && echo Warn : missing variable SSH_HOST_NAME, host verification disabled.
test -n "$SSH_HOST_NAME" && test -z "$SSH_HOST_PORT" && SSH_HOST_PORT=22 && echo Info : no variable SSH_HOST_PORT, using 22

#echo $SSH_PRIVATE_KEY
#echo $SSH_HOST_NAME
echo SSH_HOST_NAME: $SSH_HOST_NAME
echo SSH_HOST_PORT: $SSH_HOST_PORT

if [ -n "$SSH_HOST_NAME" ] ; then
    eval $(ssh-agent -s)
    export SSH_AUTH_SOCK=$SSH_AUTH_SOCK
    export SSH_AGENT_PID=$SSH_AGENT_PID
    printf '%s' "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    ssh-keyscan -p $SSH_HOST_PORT -H "$SSH_HOST_NAME" > ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts
else
    echo "$SSH_PRIVATE_KEY" > pkey && chmod 700 pkey
    ssh(){ $(which ssh) -i pkey -o StrictHostKeyChecking=no $@ ;}
fi


